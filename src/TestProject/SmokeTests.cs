using System.Linq;
using Moq;
using NUnit.Framework;
using System.Threading.Tasks;
using WarOfTheWordle.Services;
using WarOfTheWordle.Services.Game;

namespace TestProject
{
    public class SmokeTests
    {
        string client1 = "client1", player1 = "player1";
        string client2 = "client2", player2 = "player2";

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task AddPlayers()
        {
            var mockClientDispatch = new Mock<IClientDispatcher>();
            var game = new WordleGame(new WordleGameSetup { FixedWord = "hello", NumberOfTurns = 3 }, new TestWordService(), mockClientDispatch.Object);

            // Add expect pass, no previous data
            var result = await game.AddPlayerAsync(client1, player1);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Player, Is.Not.Null);
            Assert.That(result.Player.Name, Is.EqualTo(player1));

            // Re-add, expect failure
            result = await game.AddPlayerAsync(client1, player1);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Success, Is.False);
            Assert.That(result.ErrorMessage, Is.Not.Empty);
            Assert.That(result.Player, Is.Null);

            // Re-use username, expect failure
            result = await game.AddPlayerAsync(client2, player1);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Success, Is.False);
            Assert.That(result.Player, Is.Null);
            Assert.That(result.ErrorMessage, Is.Not.Empty);

            // Re-use client, expect failure
            result = await game.AddPlayerAsync(client1, player2);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Success, Is.False);
            Assert.That(result.Player, Is.Null);
            Assert.That(result.ErrorMessage, Is.Not.Empty);

            // Add expect pass, all new data
            result = await game.AddPlayerAsync(client2, player2);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Player, Is.Not.Null);
            Assert.That(result.Player.Name, Is.EqualTo(player2));
        }
     
        [Test]
        public async Task TakeTurnHandlesCorrectGuess()
        {
            string wordToGuess = "hello";
            var mockClientDispatch = new Mock<IClientDispatcher>();
            var game = new WordleGame(new WordleGameSetup { FixedWord = wordToGuess, NumberOfTurns = 3 }, new TestWordService(), mockClientDispatch.Object);

            // Add expect pass, no previous data
            var addPlayerResult = await game.AddPlayerAsync(client1, player1);
            Assert.That(addPlayerResult, Is.Not.Null);
            Assert.That(addPlayerResult.Success, Is.True);
            Assert.That(addPlayerResult.Player, Is.Not.Null);

            var result = await game.HandlePlayerTurn(client1, wordToGuess);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GuessResult, Is.Not.Null);
            Assert.True(result.GuessResult.Guesses.All(x => x == GuessResult.CorrectPossition));

            Assert.That(result.GuessAttemptResult, Is.EqualTo(GuessAttemptResult.ValidGuess));
            var player = game.GetPlayers().First(x => x.ClientId == client1);
            Assert.That(player, Is.Not.Null);
            Assert.That(player.PlayerState, Is.EqualTo(PlayerState.Won));

        }
        
        [Test]
        public async Task LastGuessIsNotALoss()
        {
            string wordToGuess = "hello";
            string guess1 = "abcde";
            string guess2 = "abcdf";
            string guess3 = "abcdg";
            string guess4 = "abcdh";
            string guess5 = "abcdi";
            
            var mockClientDispatch = new Mock<IClientDispatcher>();
            var game = new WordleGame(new WordleGameSetup { FixedWord = wordToGuess, NumberOfTurns = 6 }, new TestWordService(), mockClientDispatch.Object);

            // Add expect pass, no previous data
            var addPlayerResult = await game.AddPlayerAsync(client1, player1);
            Assert.That(addPlayerResult, Is.Not.Null);
            Assert.That(addPlayerResult.Success, Is.True);
            Assert.That(addPlayerResult.Player, Is.Not.Null);

            var result = await game.HandlePlayerTurn(client1, guess1);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GuessResult, Is.Not.Null);
            
            result = await game.HandlePlayerTurn(client1, guess2);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GuessResult, Is.Not.Null);
            
            result = await game.HandlePlayerTurn(client1, guess3);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GuessResult, Is.Not.Null);
            
            result = await game.HandlePlayerTurn(client1, guess4);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GuessResult, Is.Not.Null);

            result = await game.HandlePlayerTurn(client1, guess5);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GuessResult, Is.Not.Null);

            var player = game.GetPlayers().First(x => x.ClientId == client1);
            Assert.That(player, Is.Not.Null);
            Assert.That(player.PlayerState, Is.EqualTo(PlayerState.Playing));
            
            result = await game.HandlePlayerTurn(client1, wordToGuess);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GuessResult, Is.Not.Null);
            Assert.True(result.GuessResult.Guesses.All(x => x == GuessResult.CorrectPossition));

            Assert.That(result.GuessAttemptResult, Is.EqualTo(GuessAttemptResult.ValidGuess));
            player = game.GetPlayers().First(x => x.ClientId == client1);
            Assert.That(player, Is.Not.Null);
            Assert.That(player.PlayerState, Is.EqualTo(PlayerState.Won));
        }

        [Test]
        public async Task TakeTurnHandlesWrongPositionGuess()
        {
            string wordToGuess = "hello";
            string guess =       "lhoel";

            var mockClientDispatch = new Mock<IClientDispatcher>();
            var game = new WordleGame(new WordleGameSetup { FixedWord = wordToGuess, NumberOfTurns = 3 }, new TestWordService(), mockClientDispatch.Object);

            // Add expect pass, no previous data
            var addPlayerResult = await game.AddPlayerAsync(client1, player1);
            Assert.That(addPlayerResult, Is.Not.Null);
            Assert.That(addPlayerResult.Success, Is.True);
            Assert.That(addPlayerResult.Player, Is.Not.Null);

            var result = await game.HandlePlayerTurn(client1, guess);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GuessResult, Is.Not.Null);
            Assert.True(result.GuessResult.Guesses.All(x => x == GuessResult.WrongPossition));

            Assert.That(result.GuessAttemptResult, Is.EqualTo(GuessAttemptResult.ValidGuess));
            var player = game.GetPlayers().First(x => x.ClientId == client1);
            Assert.That(player, Is.Not.Null);
            Assert.That(player.PlayerState, Is.EqualTo(PlayerState.Playing));

        }
        
        [Test]
        public async Task TakeTurnHandlesWrongPositionGuessOverMultipleGames()
        {
            bool inSecondRound = false;            
            string round1WordToGuess = "hello";
            string round1Guess =       "lhoel";
            
            string round2WordToGuess = "smile";
            string round2Guess =       "bmeil";
            
            var wordService = new Mock<IWordService>();
            wordService.Setup(x => x.GetNewGameWord()).Returns(() => inSecondRound ? round2WordToGuess : round1WordToGuess);
            wordService.Setup(x => x.IsValidGameWord(It.IsAny<string>())).Returns(true);
            wordService.Setup(x => x.IsValidGameWordAsync(It.IsAny<string>())).Returns(() => Task.FromResult(true));

            var mockClientDispatch = new Mock<IClientDispatcher>();
            var game = new WordleGame(new WordleGameSetup { NumberOfTurns = 3 }, wordService.Object, mockClientDispatch.Object);

            // Add expect pass, no previous data
            var addPlayerResult = await game.AddPlayerAsync(client1, player1);
            Assert.That(addPlayerResult, Is.Not.Null);
            Assert.That(addPlayerResult.Success, Is.True);
            Assert.That(addPlayerResult.Player, Is.Not.Null);

            var result = await game.HandlePlayerTurn(client1, round1Guess);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GuessResult, Is.Not.Null);
            Assert.True(result.GuessResult.Guesses.All(x => x == GuessResult.WrongPossition));

            Assert.That(result.GuessAttemptResult, Is.EqualTo(GuessAttemptResult.ValidGuess));
            var player = game.GetPlayers().First(x => x.ClientId == client1);
            Assert.That(player, Is.Not.Null);
            Assert.That(player.PlayerState, Is.EqualTo(PlayerState.Playing));

            result = await game.HandlePlayerTurn(client1, round1WordToGuess);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GuessResult, Is.Not.Null);
            player = game.GetPlayers().First(x => x.ClientId == client1);
            Assert.That(player, Is.Not.Null);
            Assert.That(player.PlayerState, Is.EqualTo(PlayerState.Won));

            inSecondRound = true;
            await game.AttemptNextRound();
            result = await game.HandlePlayerTurn(client1, round2Guess);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GuessResult, Is.Not.Null);
            
            Assert.AreEqual(GuessResult.Miss, result.GuessResult.Guesses.ElementAt(0));
            Assert.AreEqual(GuessResult.CorrectPossition, result.GuessResult.Guesses.ElementAt(1));
            Assert.AreEqual(GuessResult.WrongPossition, result.GuessResult.Guesses.ElementAt(2));
            Assert.AreEqual(GuessResult.WrongPossition, result.GuessResult.Guesses.ElementAt(3));
            Assert.AreEqual(GuessResult.WrongPossition, result.GuessResult.Guesses.ElementAt(4));
            
            player = game.GetPlayers().First(x => x.ClientId == client1);
            Assert.AreEqual(1, player.KnownBadLetters.Count);
            Assert.AreEqual(1, player.KnownGoodLetters.Count);
        }

        [Test]
        public async Task TakeTurnNoFalsePositivesOnWrongPositionGuess()
        {
            string wordToGuess = "money";
            string guess =       "mooey";

            var mockClientDispatch = new Mock<IClientDispatcher>();
            var game = new WordleGame(new WordleGameSetup { FixedWord = wordToGuess, NumberOfTurns = 3 }, new TestWordService(), mockClientDispatch.Object);

            // Add expect pass, no previous data
            var addPlayerResult = await game.AddPlayerAsync(client1, player1);
            Assert.That(addPlayerResult, Is.Not.Null);
            Assert.That(addPlayerResult.Success, Is.True);
            Assert.That(addPlayerResult.Player, Is.Not.Null);

            var result = await game.HandlePlayerTurn(client1, guess);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GuessResult, Is.Not.Null);
            Assert.AreEqual(GuessResult.CorrectPossition , result.GuessResult.Guesses.ElementAt(0));
            Assert.AreEqual(GuessResult.CorrectPossition , result.GuessResult.Guesses.ElementAt(1));
            Assert.AreEqual(GuessResult.Miss , result.GuessResult.Guesses.ElementAt(2));
            Assert.AreEqual(GuessResult.CorrectPossition , result.GuessResult.Guesses.ElementAt(3));
            Assert.AreEqual(GuessResult.CorrectPossition , result.GuessResult.Guesses.ElementAt(4));
            
            // Now check the reverse situation 
            
            wordToGuess = "mooey";
            guess =       "money";

            game = new WordleGame(new WordleGameSetup { FixedWord = wordToGuess, NumberOfTurns = 3 }, new TestWordService(), mockClientDispatch.Object);

            // Add expect pass, no previous data
            addPlayerResult = await game.AddPlayerAsync(client1, player1);
            Assert.That(addPlayerResult, Is.Not.Null);
            Assert.That(addPlayerResult.Success, Is.True);
            Assert.That(addPlayerResult.Player, Is.Not.Null);

            result = await game.HandlePlayerTurn(client1, guess);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GuessResult, Is.Not.Null);
            Assert.AreEqual(GuessResult.CorrectPossition , result.GuessResult.Guesses.ElementAt(0));
            Assert.AreEqual(GuessResult.CorrectPossition , result.GuessResult.Guesses.ElementAt(1));
            Assert.AreEqual(GuessResult.Miss , result.GuessResult.Guesses.ElementAt(2));
            Assert.AreEqual(GuessResult.CorrectPossition , result.GuessResult.Guesses.ElementAt(3));
            Assert.AreEqual(GuessResult.CorrectPossition , result.GuessResult.Guesses.ElementAt(4));
            
            
            wordToGuess = "moxoy";
            guess =       "moneo";

            game = new WordleGame(new WordleGameSetup { FixedWord = wordToGuess, NumberOfTurns = 3 }, new TestWordService(), mockClientDispatch.Object);

            // Add expect pass, no previous data
            addPlayerResult = await game.AddPlayerAsync(client1, player1);
            Assert.That(addPlayerResult, Is.Not.Null);
            Assert.That(addPlayerResult.Success, Is.True);
            Assert.That(addPlayerResult.Player, Is.Not.Null);

            result = await game.HandlePlayerTurn(client1, guess);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GuessResult, Is.Not.Null);
            Assert.AreEqual(GuessResult.CorrectPossition , result.GuessResult.Guesses.ElementAt(0));
            Assert.AreEqual(GuessResult.CorrectPossition , result.GuessResult.Guesses.ElementAt(1));
            Assert.AreEqual(GuessResult.Miss , result.GuessResult.Guesses.ElementAt(2));
            Assert.AreEqual(GuessResult.Miss , result.GuessResult.Guesses.ElementAt(3));
            Assert.AreEqual(GuessResult.WrongPossition , result.GuessResult.Guesses.ElementAt(4));
            Assert.True(result.Player.KnownGoodLetters.Contains("o")); 
            Assert.False(result.Player.KnownBadLetters.Contains("o")); //not a bad letter, even if it was a miss
        }

        [Test]
        public async Task TakeTurnHandlesWrongGuess()
        {
            string wordToGuess = "hello";
            string guess = "angry";

            var mockClientDispatch = new Mock<IClientDispatcher>();
            var game = new WordleGame(new WordleGameSetup { FixedWord = wordToGuess, NumberOfTurns = 3 }, new TestWordService(), mockClientDispatch.Object);

            // Add expect pass, no previous data
            var addPlayerResult = await game.AddPlayerAsync(client1, player1);
            Assert.That(addPlayerResult, Is.Not.Null);
            Assert.That(addPlayerResult.Success, Is.True);
            Assert.That(addPlayerResult.Player, Is.Not.Null);

            var result = await game.HandlePlayerTurn(client1, guess);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GuessResult, Is.Not.Null);
            Assert.True(result.GuessResult.Guesses.All(x => x == GuessResult.Miss));

            Assert.That(result.GuessAttemptResult, Is.EqualTo(GuessAttemptResult.ValidGuess));
            var player = game.GetPlayers().First(x => x.ClientId == client1);
            Assert.That(player, Is.Not.Null);
            Assert.That(player.PlayerState, Is.EqualTo(PlayerState.Playing));
        }

        [Test]
        public async Task TakeTurnHandlesAllStatusGuess()
        {
            string wordToGuess = "hello";
            string guess =       "hovel";

            var mockClientDispatch = new Mock<IClientDispatcher>();
            var game = new WordleGame(new WordleGameSetup { FixedWord = wordToGuess, NumberOfTurns = 3 }, new TestWordService(), mockClientDispatch.Object);

            // Add expect pass, no previous data
            var addPlayerResult = await game.AddPlayerAsync(client1, player1);
            Assert.That(addPlayerResult, Is.Not.Null);
            Assert.That(addPlayerResult.Success, Is.True);
            Assert.That(addPlayerResult.Player, Is.Not.Null);

            var result = await game.HandlePlayerTurn(client1, guess);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GuessResult, Is.Not.Null);
            Assert.That(result.GuessAttemptResult, Is.EqualTo(GuessAttemptResult.ValidGuess));

            var lastGuessRound = result.GuessResult.Guesses;

            Assert.AreEqual(lastGuessRound.ElementAt(0),GuessResult.CorrectPossition); // h
            Assert.AreEqual(lastGuessRound.ElementAt(1),GuessResult.WrongPossition); // o
            Assert.AreEqual(lastGuessRound.ElementAt(2),GuessResult.Miss); // v
            Assert.AreEqual(lastGuessRound.ElementAt(3),GuessResult.WrongPossition); // e
            Assert.AreEqual(lastGuessRound.ElementAt(4),GuessResult.WrongPossition); // l

            var player = game.GetPlayers().First(x => x.ClientId == client1);
            Assert.That(player, Is.Not.Null);
            Assert.That(player.PlayerState, Is.EqualTo(PlayerState.Playing));

        }

        [Test]
        public async Task NoTurnTakenWithInvalidWord()
        {
            string wordToGuess = "hello";
            string guess = "angry";

            var mockWordService = new Mock<IWordService>();
            mockWordService.Setup(x => x.IsValidGameWord(It.IsAny<string>())).Returns(false);
            mockWordService.Setup(x => x.IsValidGameWordAsync(It.IsAny<string>())).Returns(async () => false);

            var mockClientDispatch = new Mock<IClientDispatcher>();

            var game = new WordleGame(new WordleGameSetup { FixedWord = wordToGuess, NumberOfTurns = 3 }, mockWordService.Object, mockClientDispatch.Object);

            // Add expect pass, no previous data
            var addPlayerResult = await game.AddPlayerAsync(client1, player1);
            Assert.That(addPlayerResult, Is.Not.Null);
            Assert.That(addPlayerResult.Success, Is.True);
            Assert.That(addPlayerResult.Player, Is.Not.Null);

            var result = await game.HandlePlayerTurn(client1, guess);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GuessAttemptResult, Is.EqualTo(GuessAttemptResult.BadWord));
            Assert.That(result.GuessResult, Is.Null);

            var player = game.GetPlayers().First(x => x.ClientId == client1);
            Assert.That(player, Is.Not.Null);
            Assert.That(player.PlayerGuesses, Is.Empty);
            Assert.That(player.PlayerState, Is.EqualTo(PlayerState.Playing));

        }

        [Test]
        public async Task PlayersHaveCorrectStateAfterCorrectGuess()
        {
            string wordToGuess = "hello";
            var mockClientDispatch = new Mock<IClientDispatcher>();
            var game = new WordleGame(new WordleGameSetup { FixedWord = wordToGuess, NumberOfTurns = 3 }, new TestWordService(), mockClientDispatch.Object);

            // Add expect pass, no previous data
            var addPlayerResult = await game.AddPlayerAsync(client1, this.player1);
            Assert.That(addPlayerResult, Is.Not.Null);
            Assert.That(addPlayerResult.Success, Is.True);
            Assert.That(addPlayerResult.Player, Is.Not.Null);

            addPlayerResult = await game.AddPlayerAsync(client2, player2);
            Assert.That(addPlayerResult, Is.Not.Null);
            Assert.That(addPlayerResult.Success, Is.True);
            Assert.That(addPlayerResult.Player, Is.Not.Null);

            var result = await game.HandlePlayerTurn(client1, wordToGuess);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GuessResult, Is.Not.Null);
            Assert.True(result.GuessResult.Guesses.All(x => x == GuessResult.CorrectPossition));

            Assert.That(result.GuessAttemptResult, Is.EqualTo(GuessAttemptResult.ValidGuess));
            var p1 = game.GetPlayers().First(x => x.ClientId == client1);
            Assert.That(p1, Is.Not.Null);
            Assert.That(p1.PlayerState, Is.EqualTo(PlayerState.Won));

            var p2 = game.GetPlayers().First(x => x.ClientId == client2);
            Assert.That(p2, Is.Not.Null);
            Assert.That(p2.PlayerState, Is.EqualTo(PlayerState.Lost));

        }
    }
}