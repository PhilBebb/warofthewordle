﻿using WarOfTheWordle.Hubs;

namespace WarOfTheWordle.Constants
{
    public class SignalRConstants
    {
        public const string HubAddress = "/gamehub";

        public const string GameNotFoundMessage = "GameNotFound";

        public const string AddedPlayerMessage = "AddedPlayer";
        public const string FailedToAddPlayerMessage = "FailedToAddPlayer";
        public const string NewOppoentMessage = "NewOppoent";

        public const string PlayerTookTurnMessage = "PlayerTookTurn";
        public const string OtherPlayerTookTurnMessage = "OtherPlayerTookTurn";

        public const string CurrentPlayerWonTurnMessage = "CurrentPlayerWonTurn";
        public const string OtherPlayerWonTurnMessage = "OtherPlayerWonTurn";
        public const string AllPlayersLostMessage = "AllPlayersLos";
        public const string CurentPlayerLostMessage = "CuurentPlayersLost";

        public const string StartNewRoundMessage = "StartNewRound";
        public const string GameOverMessage = "GameOver";
              
        public const string AttemptToRegisterPlayerMessage = nameof(GameHub.AttemptRegisterPlayer);
        public const string TakePlayerTurnMessage = nameof(GameHub.TakePlayerTurn);
        public const string AttempNewRoundMessage = nameof(GameHub.AttemptNewRound);
    }


}
