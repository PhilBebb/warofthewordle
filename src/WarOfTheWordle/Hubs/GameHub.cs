﻿using Microsoft.AspNetCore.SignalR;
using WarOfTheWordle.Services.Game;

namespace WarOfTheWordle.Hubs
{
    public class GameHub : Hub
    {
        private readonly IGameService _gameService;

        public GameHub(IGameService gameService)
        {
            this._gameService = gameService;
        }

        private async Task HandleGameAction(Guid gameId, Func<IWordleGame, Task> func)
        {
            IWordleGame? game = GetGame(gameId);
            if (game is { })
            {
                await func(game);
            }
            else
            {
                await Clients.Client(Context.ConnectionId).SendAsync(Constants.SignalRConstants.GameNotFoundMessage, new[] { gameId });
            }

        }

        private IWordleGame? GetGame(Guid gameId)
        {
            return _gameService.GetGameByGuid(gameId);
        }

        public async Task AttemptRegisterPlayer(Guid gameId, string playerName) => await HandleGameAction(gameId, (_wordleGame) => _wordleGame.AddPlayerAsync(base.Context.ConnectionId, playerName));

        public async Task TakePlayerTurn(Guid gameId, string guess) => await HandleGameAction(gameId, (_wordleGame) => _wordleGame.HandlePlayerTurn(base.Context.ConnectionId, guess));
        public async Task AttemptNewRound(Guid gameId) => await HandleGameAction(gameId, (_wordleGame) => _wordleGame.AttemptNextRound());

        public override Task OnConnectedAsync()
        {
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception? exception) => _gameService.NotifyClientDisconnect(Context.ConnectionId);
    }
}
