namespace WarOfTheWordle.Web;

public enum KeyPress
{
    Enter,
    Backspace,
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z
}

public static class KeyPressExtension
{
    private static List<KeyPress> _eventKeys => new() {KeyPress.Enter, KeyPress.Backspace};
    private static List<List<KeyPress>> _defaultSetup => new()
    {
        new(){KeyPress.Q, KeyPress.W, KeyPress.E, KeyPress.R, KeyPress.T, KeyPress.Y, KeyPress.U, KeyPress.I, KeyPress.O, KeyPress.P},
        new(){KeyPress.A, KeyPress.S, KeyPress.D, KeyPress.F, KeyPress.G, KeyPress.H, KeyPress.J, KeyPress.K, KeyPress.L, KeyPress.Enter},
        new(){KeyPress.Z, KeyPress.X, KeyPress.C, KeyPress.V, KeyPress.B, KeyPress.N, KeyPress.M, KeyPress.Backspace},
    };

    public static bool IsEventKey(this KeyPress key) => _eventKeys.Contains(key);
    
    public static List<List<KeyPress>> DefaultSetup => _defaultSetup;

    public static IReadOnlyCollection<KeyPress> ToKeyPress(ICollection<string> knownBadLetters)
    {
        var retval = new List<KeyPress>(knownBadLetters.Count());
        foreach (var letter in knownBadLetters)
        {
            retval.Add(Enum.Parse<KeyPress>(letter));
        }

        return retval;
    }
}