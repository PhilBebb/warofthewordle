﻿using WarOfTheWordle.Web;

namespace WarOfTheWordle.Services.Game
{
    public class Player
    {
        public Player()
        {

        }

        public Player(string name, string clientId)
        {
            Name = name;
            ClientId = clientId;

            SetToWatching();
        }

        public string Name { get; set; }
        public string ClientId { get; set; }
        public PlayerState PlayerState { get; set; }

        public ICollection<CurrentPlayerGuess> PlayerGuesses { get; set; } = new List<CurrentPlayerGuess>();
        public ICollection<string> KnownGoodLetters { get; set; } = new List<string>();
        public ICollection<string> KnownBadLetters { get; set; } = new List<string>();

        public IReadOnlyCollection<KeyPress> KnownBadKeys => KeyPressExtension.ToKeyPress(KnownBadLetters);

        public void AddTurn(CurrentPlayerGuess guess)
        {
            PlayerGuesses.Add(guess);

            for (int i = 0; i < guess.Guess.Length; i++)
            {
                var letter = guess.Guess[i].ToString();
                var letterState = guess.Guesses.ElementAt(i);

                if (GuessResult.CorrectPossition == letterState)
                {
                    KnownGoodLetters.Add(letter);
                }
                
                if (GuessResult.Miss == letterState && !KnownGoodLetters.Contains(letter))
                {
                    KnownBadLetters.Add(letter);
                }
            }

            KnownBadLetters = KnownBadLetters.Distinct().ToList();
        }

        public void SetToPlaying() => PlayerState = PlayerState.Playing;
        public void SetToWon() => PlayerState = PlayerState.Won;
        public void SetToLost() => PlayerState = PlayerState.Lost;
        public void SetToWatching() => PlayerState = PlayerState.Watching;

        public CurrentPlayerGuess? MostRecentGuess => PlayerGuesses?.LastOrDefault();

        internal void StartNextRound()
        {
            SetToWatching();
            KnownBadLetters.Clear();
            KnownGoodLetters.Clear();
            PlayerGuesses.Clear();
        }
    }

    public enum PlayerState
    {
        Playing, Lost, Won, Watching
    }
}
