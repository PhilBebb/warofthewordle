﻿namespace WarOfTheWordle.Services.Game
{
    public class WordleGameSetup
    {
        public static uint InfiniteRounds => 0;
        public static uint DefaultRounds => 3;
        public static uint DefaultTurns => 6;
        public static string UseRandomWord => string.Empty;
        public static string DefaultGameName => string.Empty;
        
        public uint NumberOfTurns { get; set; } = DefaultRounds;
        public uint NumberOfRounds { get; set; } = DefaultTurns;
        public string FixedWord { get; set; } = UseRandomWord;
        public string GameName { get; set; } = DefaultGameName;
    }
}
