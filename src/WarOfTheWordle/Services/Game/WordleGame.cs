﻿using System.Security.Claims;
using System.Collections.Concurrent;
using WarOfTheWordle.Constants;

namespace WarOfTheWordle.Services.Game
{

    public interface IWordleGame
    {
        Guid GetGameId();
        string GetName();
        IReadOnlyCollection<Player> GetPlayers();
        Task<PlayerAddResult> AddPlayerAsync(string clientId, string playerName);
        Task<PlayerTurnResult> HandlePlayerTurn(string clientId, string word);
        Task AttemptNextRound();
        Task NotifyClientDisconnect(string clientId);
    }

    public class WordleGame : IWordleGame
    {
        private readonly object _lock = new ();

        private readonly IWordService _wordService;
        private readonly IClientDispatcher _clientDispatcher;
        private string _activeWord = string.Empty;
        private string _normalisedActiveWord => _activeWord.ToLowerInvariant();
        public uint Turns { get; set; }
        public uint CurrentRound { get; private set; }
        public uint NumberOfRounds { get; set; }
        public string GameName { get; set; } = string.Empty; public string GetName() => GameName;
        public Guid GameId = Guid.NewGuid(); public Guid GetGameId() => GameId;

        private ConcurrentBag<Player> _players = new();

        public WordleGame(WordleGameSetup gameSetup, IWordService wordService, IClientDispatcher clientDispatcher)
        {
            _wordService = wordService;
            _clientDispatcher = clientDispatcher;

            GameName = gameSetup.GameName;
            
            NumberOfRounds = gameSetup.NumberOfRounds;
           
            CurrentRound = 1;

            Turns = gameSetup.NumberOfTurns;
           
            if (!string.IsNullOrWhiteSpace(gameSetup.FixedWord)){
                NumberOfRounds = 1;
                _activeWord = gameSetup.FixedWord;
            }
            else
            {
                _activeWord = _wordService.GetNewGameWord();
            }
        }

        public async Task<PlayerAddResult> AddPlayerAsync(string clientId, string playerName)
        {            
            PlayerAddResult retval = null;

            lock (_lock)
            {
                bool failure = false;
                string failMessage = string.Empty;

                if (string.IsNullOrWhiteSpace(playerName))
                {
                    failMessage = "If you don't tell me you're name, I won't let you play!";
                    failure = true;
                }
                
                if (_players.Any(x => string.Equals(x.ClientId, clientId))){
                    failMessage = "You are already in a game";
                    failure = true;
                }

                if (!failure && _players.Any(x => string.Equals(x.Name, playerName)))
                {
                    failMessage = $"{playerName} is already in use";
                    failure = true;
                }
                
                if (!failure && playerName.Length >= 20)
                {
                    failMessage = $"{playerName} is too long";
                    failure = true;
                }

                if (failure)
                {
                    retval = PlayerAddResult.FailedToAdd(clientId, failMessage);
                }
                else
                {
                    var player = new Player(playerName, clientId);
                    _players.Add(player);
                    retval = PlayerAddResult.SuccessfullyAdded(player, GetServerStateForPlayer(player));
                }
            }

            if (retval.Success && retval.Player is { })
            {
                await HandlePlayerConnected(retval); 
            }
            else
            {
                await HandlePlayerRejectd(retval);
            }

            return retval;
        }

        private async Task HandlePlayerConnected(PlayerAddResult newPlayer)
        {
            await _clientDispatcher.SendToClient(newPlayer.Player.ClientId, new CLientMessage<PlayerAddResult>(SignalRConstants.AddedPlayerMessage, newPlayer));
            await _clientDispatcher.SendToAllButClient(newPlayer.Player.ClientId, new CLientMessage<string>(SignalRConstants.NewOppoentMessage, newPlayer.Player.Name));
        }
        
        private async Task HandlePlayerRejectd(PlayerAddResult rejectedPlayer)
        {
            await _clientDispatcher.SendToClient(rejectedPlayer.ClientId, new CLientMessage<PlayerAddResult>(SignalRConstants.FailedToAddPlayerMessage, rejectedPlayer));
        }

        public IReadOnlyCollection<Player> GetPlayers()
        {
            lock (_lock)
            {
                return _players;
            }
        }

        public async Task<PlayerTurnResult> HandlePlayerTurn(string clientId, string word)
        {
            Player player = null;

            lock (_lock)
            {
                player = _players.FirstOrDefault(x => string.Equals(x.ClientId, clientId));              
            }
            
            if (player is null)
            {
                return PlayerTurnResult.InvlaidGuess(null, GuessAttemptResult.Other);
            }

            if ((player.PlayerGuesses?.Count ?? 0) >= Turns)
            {
                player.SetToLost();
                return PlayerTurnResult.InvlaidGuess(player, GuessAttemptResult.AlreadyLost);
            }

            player.SetToPlaying();

            PlayerTurnResult? playerGuessResult = await CheckPlayerGuess(word, player);
            if (playerGuessResult.GuessAttemptResult == GuessAttemptResult.ValidGuess)
            {
                player.AddTurn(playerGuessResult.GuessResult);

                // check for victory
                if (playerGuessResult.GuessResult.Guesses.All(x => GuessResult.CorrectPossition == x))
                {
                    lock (_lock)
                    {
                        if (!_players.Any(x => x.ClientId != player.ClientId && player.PlayerState == PlayerState.Won))
                        {
                            player.SetToWon();
                        }

                        var loosingPlayers = _players.Except(new[] { player }).ToList();
                        loosingPlayers.ForEach(x => x.SetToLost());
                    }
                }
                if (PlayerState.Won != player.PlayerState && (player.PlayerGuesses?.Count ?? 0) >= Turns)
                {
                    player.SetToLost(); //will notify of loss later
                }
            }

            // let the players know now about the guess, victory/failure comes later
            await NotifyPlayerTurn(player, playerGuessResult);

            await CheckForGameEnd();

            return playerGuessResult;
        }

        private Task CheckForGameEnd()
        {
            List<Player> state = null;
            List<Task> tasks = new();

            lock (_lock)
            {
                state = _players.ToList();
            }

            var winningPlayer = state.SingleOrDefault(x => PlayerState.Won == x.PlayerState);
            if (winningPlayer is { })
            {
                tasks.Add(_clientDispatcher.SendToAllButClient(winningPlayer.ClientId, new CLientMessage<Player>(SignalRConstants.OtherPlayerWonTurnMessage, winningPlayer)));
                tasks.Add(_clientDispatcher.SendToClient(winningPlayer.ClientId, new CLientMessage<Player>(SignalRConstants.CurrentPlayerWonTurnMessage, winningPlayer)));
            } else if (state.All(x => PlayerState.Lost == x.PlayerState))
            {
                var allPayersLostState = (_activeWord, _players.ToList());
                tasks.Add(_clientDispatcher.SendToAll(new CLientMessage<(string, List<Player>)> (SignalRConstants.AllPlayersLostMessage, allPayersLostState)));
            }
            else
            {
                tasks.AddRange(state.Where(x => x.PlayerState == PlayerState.Lost).ToList().Select(x => _clientDispatcher.SendToClient(x.ClientId, new CLientMessage<object>(SignalRConstants.CurentPlayerLostMessage, null))));
            }


            return Task.WhenAll(tasks);
        }

        private Task NotifyPlayerTurn(Player player, PlayerTurnResult playerGuessResult)
        {
            List<Task> tasks = new List<Task>
            {
                //_clientDispatcher.SendToClient(player.ClientId, new CLientMessage<PlayerTurnResult>(SignalRConstants.PlayerTookTurnMessage, playerGuessResult))
                _clientDispatcher.SendToClient(player.ClientId, new CLientMessage<ServerSidePlayerResonseState>(SignalRConstants.PlayerTookTurnMessage, new ServerSidePlayerResonseState{ Player = player, TurnResult = playerGuessResult }))
            };

            if (GuessAttemptResult.ValidGuess == playerGuessResult.GuessAttemptResult)
            {

                List<Player> allPlayers = null;
                lock (_lock)
                {
                    allPlayers = _players.ToList();
                }

                var playersToNotify = allPlayers.Except(new[] { player }).ToList();

                foreach (var playerToNotify in playersToNotify)
                {
                    var playerState = GetServerStateForPlayer(playerToNotify);
                    if (playerState.Opponents.Any(o => o.PlayerGuesses.Any()))
                    {
                        tasks.Add(
                            _clientDispatcher.SendToClient(playerToNotify.ClientId, new CLientMessage<ServerSideOpponentPlayerState>(SignalRConstants.OtherPlayerTookTurnMessage, playerState))
                        );
                    }                    
                }
            }

            return Task.WhenAll(tasks);
        }

        private ServerSideOpponentPlayerState GetServerStateForPlayer(Player currentPlayer)
        {
            ServerSideOpponentPlayerState retval = new ServerSideOpponentPlayerState();

            List<Player> allPlayers = null;
            lock (_lock)
            {
                allPlayers = _players.ToList();
            }
            allPlayers = allPlayers.Where(x => x.PlayerGuesses.Any()).ToList();

            var playersThisPlayerCanSee = allPlayers.Except(new[] { currentPlayer }).ToList();
            retval.AddPlayerData(playersThisPlayerCanSee);
            return retval;
        }

        private async Task <PlayerTurnResult> CheckPlayerGuess(string guessedWord, Player guesser)
        {
            if (_normalisedActiveWord.Length != guessedWord.Length)
            {
                return PlayerTurnResult.InvlaidGuess(guesser, GuessAttemptResult.BadData);
            }

            if (guesser.PlayerGuesses.Any(pGuess => string.Equals(guessedWord, pGuess.Guess, StringComparison.InvariantCultureIgnoreCase)))
            {
                return PlayerTurnResult.InvlaidGuess(guesser, GuessAttemptResult.RepeateWord);
            }

            if (!await _wordService.IsValidGameWordAsync(guessedWord))
            {
                return PlayerTurnResult.InvlaidGuess(guesser, GuessAttemptResult.BadWord);
            }

            List<GuessResult> letterStatus = new(5);

            for (int letterPos = 0; letterPos < _normalisedActiveWord.Length; letterPos++)
            {
                GuessResult thisLetterResult = GuessResult.Miss;
                var letterToGuess = _normalisedActiveWord.ElementAt(letterPos).ToString();
                var currentLetter = guessedWord.ElementAt(letterPos).ToString().ToLower();

                if (string.Equals(letterToGuess, currentLetter, StringComparison.InvariantCultureIgnoreCase))
                {
                    thisLetterResult = GuessResult.CorrectPossition;
                }else if (_normalisedActiveWord.Contains(currentLetter))
                {
                    thisLetterResult = GuessResult.WrongPossition;
                }

                letterStatus.Add(thisLetterResult);
            }

            // remove false positives for a wrong letter
            for (int letterPos = 0; letterPos < _normalisedActiveWord.Length; letterPos++)
            {
                var letterGuessResult = letterStatus.ElementAt(letterPos);

                if (letterGuessResult != GuessResult.WrongPossition) continue;
                
                var currentLetter = guessedWord.ElementAt(letterPos).ToString().ToLower();

                if (_normalisedActiveWord.Count(c => c.ToString().ToLower().Equals(currentLetter)) != 1) continue;

                int expectedPosition = _normalisedActiveWord.IndexOf(currentLetter);
                if (GuessResult.CorrectPossition == letterStatus.ElementAt(expectedPosition))
                {
                    letterStatus[letterPos] = GuessResult.Miss;    
                }
            }

            uint turn = (uint)guesser.PlayerGuesses.Count+1;
            CurrentPlayerGuess guess = new(guesser.Name, turn, guessedWord, letterStatus);

            return PlayerTurnResult.SuccessfulGuess(guesser, guess);
        }

        public Task AttemptNextRound()
        {
            var tasks = new List<Task>();

            lock (_lock)
            {
                if (NumberOfRounds == WordleGameSetup.InfiniteRounds || CurrentRound < NumberOfRounds)
                {
                    if (_players.All(p => PlayerState.Lost == p.PlayerState) || _players.Any(p => PlayerState.Won == p.PlayerState))
                    {
                        foreach (var player in _players)
                        {
                            player.StartNextRound();
                            tasks.Add(_clientDispatcher.SendToClient(player.ClientId, new CLientMessage<Player>(SignalRConstants.StartNewRoundMessage, player)));
                        }

                        CurrentRound++;
                        _activeWord = _wordService.GetNewGameWord();
                    }                  
                }else {
                    tasks.Add(_clientDispatcher.SendToAll(new CLientMessage<object>(SignalRConstants.GameOverMessage, null)));
                }                                
            }

            return Task.WhenAll(tasks);
        }

        public Task NotifyClientDisconnect(string clientId)
        {
            bool removed = false;
            lock (_lock)
            {
                var player = _players.FirstOrDefault(x => x.ClientId == clientId);
                if (player is { })
                {
                    removed = _players.TryTake(out player);
                }
            }

            if (!removed) return Task.CompletedTask;

            return CheckForGameEnd();
        }
    }

    public class PlayerAddResult
    {
        public PlayerAddResult(){ ErrorMessage = string.Empty; }

        public static PlayerAddResult FailedToAdd(string clientId, string errorMessage) => new PlayerAddResult
        {
            ErrorMessage = errorMessage,
            Player = null,
            Success = false,
            ClientId = clientId,
        };

        public static PlayerAddResult SuccessfullyAdded(Player player, ServerSideOpponentPlayerState state) => new PlayerAddResult
        {
            ErrorMessage = string.Empty,
            Player = player,
            Success = true,
            State = state,
            ClientId = player.ClientId
        };

        public bool Success { get; set; }
        public string ClientId { get; set; }

        public string ErrorMessage { get; set; }
        public Player? Player { get; set; }
        public ServerSideOpponentPlayerState State { get; set; }
    }

    public class PlayerTurnResult
    {
        public PlayerTurnResult(){}

        public static PlayerTurnResult SuccessfulGuess(Player player, CurrentPlayerGuess guessResult) => new PlayerTurnResult { GuessAttemptResult = GuessAttemptResult.ValidGuess, Player = player, GuessResult = guessResult };
        public static PlayerTurnResult InvlaidGuess(Player player, GuessAttemptResult guessResult) => new PlayerTurnResult { GuessAttemptResult = guessResult, Player = player, GuessResult = null };


        public Player Player { get; set; }
        
        public CurrentPlayerGuess? GuessResult { get; set; } 
        public GuessAttemptResult GuessAttemptResult { get; set; }
    }

    public class ServerSidePlayerResonseState
    {
        public Player Player { get; set; }
        public PlayerTurnResult TurnResult { get; set; }
    }

    public class ServerSideOpponentPlayerState
    {
        public ICollection<ServerOpponent> Opponents { get; set; } = new List<ServerOpponent>();

        public void AddPlayerData(Player player) => Opponents.Add(ServerOpponent.FromPlayer(player));
        
        public void AddPlayerData(IEnumerable<Player> players)
        {
            foreach (Player player in players) {  AddPlayerData(player); }
        }

    }

    public class ServerOpponent
    {
        public ServerOpponent(){}

        public static ServerOpponent FromPlayer(Player player) => new ServerOpponent
        {
            State = player.PlayerState,
            Name = player.Name,
            PlayerGuesses = player.PlayerGuesses.Select(OtherPlayerGuess.FromCurrentPlayerGuess).ToArray(),
        };

        public string Name { get; set; }
        public PlayerState State { get; set; }
        public IReadOnlyCollection<OtherPlayerGuess> PlayerGuesses { get; set; }
}

}
