﻿namespace WarOfTheWordle.Services.Game
{
    public abstract class PlayerGuess
    {
        public PlayerGuess()
        {

        }

        public PlayerGuess(string playerName, uint turn, IEnumerable<GuessResult> guesses)
        {
            PlayerName = playerName;
            Turn = turn;
            Guesses = guesses.Take(5).ToArray();
        }

        public uint Turn { get; set; }
        public IReadOnlyCollection<GuessResult> Guesses { get; set; }

        public abstract string Guess { get; set; }
        public string PlayerName { get; set; }
    }

    public enum GuessResult
    {
        Miss = 0, CorrectPossition = 1, WrongPossition = 2,
    }

    public class CurrentPlayerGuess : PlayerGuess
    {
        public CurrentPlayerGuess()
        {

        }

        public CurrentPlayerGuess(string playerName, uint turn, string guess, IEnumerable<GuessResult> guesses) : base(playerName, turn, guesses)
        {
            Guess = guess;
        }

        public override string Guess { get; set; }
    }

    public class OtherPlayerGuess : PlayerGuess
    {
        public OtherPlayerGuess(string playerName, uint turn, IEnumerable<GuessResult> guesses) : base(playerName, turn, guesses) { }

        public override string Guess { get => "?????"; set { } }

        public static OtherPlayerGuess FromCurrentPlayerGuess(CurrentPlayerGuess currentPlayerGuess) => new OtherPlayerGuess(currentPlayerGuess.PlayerName, currentPlayerGuess.Turn, currentPlayerGuess.Guesses);
    }

    public enum GuessAttemptResult
    {
        ValidGuess = 0, BadWord = 1, RepeateWord = 2, BadData = 3, AlreadyLost = 4, GameIsOver = 5, Other = 999
    }


}
