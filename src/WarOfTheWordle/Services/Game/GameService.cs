﻿namespace WarOfTheWordle.Services.Game
{

    public interface IGameService
    {
        IWordleGame CreateNewGane(WordleGameSetup gameSetup);
        IReadOnlyCollection<IWordleGame> GetRunningGames();
        IWordleGame? GetGameByGuid(Guid guid);
        Task NotifyClientDisconnect(string clientId);
    }

    public class SingleGameService : IGameService
    {
        private WordleGameSetup _defaultSetup = new WordleGameSetup
        {
            GameName = string.Empty,
            NumberOfRounds = WordleGameSetup.InfiniteRounds,
            NumberOfTurns =  WordleGameSetup.DefaultTurns,
            FixedWord = WordleGameSetup.UseRandomWord,
        };
        
        private readonly IServiceProvider _serviceProvider;
        private IWordleGame _instance;

        public SingleGameService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IWordleGame CreateNewGane(WordleGameSetup gameSetup)
        {
            lock (this)
            {
                if (_instance == null)
                {
                    IClientDispatcher dispatcher = _serviceProvider.GetRequiredService<IClientDispatcher>();
                    IWordService wordService = _serviceProvider.GetRequiredService<IWordService>();

                    _instance = new WordleGame(gameSetup, wordService, dispatcher);
                }
            }
                        
            return _instance;
        }

        public IWordleGame? GetGameByGuid(Guid _) => _instance ?? CreateNewGane(_defaultSetup);

        public IReadOnlyCollection<IWordleGame> GetRunningGames() => new [] { _instance };

        public Task NotifyClientDisconnect(string clientId) => _instance?.NotifyClientDisconnect(clientId) ?? Task.CompletedTask;
    }

    public class GameService : IGameService
    {
        public IWordleGame CreateNewGane(WordleGameSetup gameSetup)
        {
            throw new NotImplementedException();
        }

        public IWordleGame? GetGameByGuid(Guid guid)
        {
            throw new NotImplementedException();
        }

        public IReadOnlyCollection<IWordleGame> GetRunningGames()
        {
            throw new NotImplementedException();
        }

        public Task NotifyClientDisconnect(string clientId)
        {
            throw new NotImplementedException();
        }
    }
}
