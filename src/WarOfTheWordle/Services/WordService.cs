﻿namespace WarOfTheWordle.Services
{
    public interface IWordService
    {
        string GetNewGameWord();
        Task<string> GetNewGameWordAsync();
        bool IsValidGameWord(string word);
        Task<bool> IsValidGameWordAsync(string word);
    }

    public class TestWordService : IWordService
    {
        public string GetNewGameWord() => "Hello";
        public Task<string> GetNewGameWordAsync() => Task.FromResult(GetNewGameWord());

        public bool IsValidGameWord(string word) => true;
        public Task<bool> IsValidGameWordAsync(string word) => Task.FromResult(IsValidGameWord(word));
    }

    public class FileBackedWordService : IWordService
    {
        private Random random = new ();
        
        private static readonly Lazy<IReadOnlyCollection<string>> Words = new(() =>
        {
            var path = Path.Combine(Path.GetDirectoryName(typeof(TestWordService).Assembly.Location), "Data", "words.txt");
            var words = File.ReadAllLines(path);
            return words;
        });

        public string GetNewGameWord()
        {
            string wordToUse = string.Empty;
            do
            {
                wordToUse = Words.Value.ElementAt(random.Next(Words.Value.Count - 1));    
            } while (wordToUse.Length != 5);

            return wordToUse;
        }

        public Task<string> GetNewGameWordAsync() => Task.FromResult(GetNewGameWord());

        public bool IsValidGameWord(string word) => Words.Value.Any(w => string.Equals(word, w, StringComparison.InvariantCultureIgnoreCase));
        public Task<bool> IsValidGameWordAsync(string word) => Task.FromResult(IsValidGameWord(word));
    }
}
