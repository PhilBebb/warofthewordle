﻿using Microsoft.AspNetCore.SignalR;
using WarOfTheWordle.Hubs;

namespace WarOfTheWordle.Services
{
    public class CLientMessage<T>
    {
        public CLientMessage(string clientMethod, T payload)
        {
            ClientMethod = clientMethod;
            Payload = payload;
        }

        public string ClientMethod { get; private set; }
        public T Payload { get; private set; }
    }

    public interface IClientDispatcher
    {
        Task RegisterClient(string clientId);
        Task DeRegisterClient(string clientId);
        Task SendToAll<T>(CLientMessage<T> message);
        Task SendToClient<T>(string clientId, CLientMessage<T> message);
        Task SendToAllButClient<T>(string clientId, CLientMessage<T> message);
        Task SendToAllButClient<T>(ICollection<string> clientId, CLientMessage<T> message);
    }

    public class SignalClientDispatcher : IClientDispatcher
    {
        //public SignalClientDispatcher(IHubContext<GameHub> context)
        public SignalClientDispatcher(GameHub context)
        {
            _context = context;
        }

        //private readonly IHubContext<GameHub> _context;
        private readonly GameHub _context;

        private Task Send<T>(IClientProxy proxy, CLientMessage<T> message) => proxy.SendAsync(message.ClientMethod, message.Payload);

        public Task SendToAll<T>(CLientMessage<T> message) => _context.Clients.All.SendAsync(message.ClientMethod, message.Payload);

        public Task SendToAllButClient<T>(string clientId, CLientMessage<T> message) => Send(_context.Clients.AllExcept(clientId), message);

        public Task SendToAllButClient<T>(ICollection<string> clientIds, CLientMessage<T> message) => Send(_context.Clients.AllExcept(clientIds), message);

        public Task SendToClient<T>(string clientId, CLientMessage<T> message) => Send(_context.Clients.Client(clientId), message);

        public Task RegisterClient(string clientId) => Task.CompletedTask;
        public Task DeRegisterClient(string clientId) => Task.CompletedTask;
    }

    public class SignalRGroupesClientDispatcher : IClientDispatcher
    {
        public SignalRGroupesClientDispatcher(string groupName, IHubContext<GameHub> context)
        {
            _groupName = groupName;
            _context = context;
        }

        private readonly string _groupName;
        private readonly IHubContext<GameHub> _context;

        private Task Send<T>(IClientProxy proxy, CLientMessage<T> message) => proxy.SendAsync(message.ClientMethod, message.Payload);

        public Task SendToAll<T>(CLientMessage<T> message) => Send(_context.Clients.Group(_groupName), message);

        public Task SendToAllButClient<T>(string clientId, CLientMessage<T> message) => Send(_context.Clients.GroupExcept(_groupName, clientId), message);

        public Task SendToAllButClient<T>(ICollection<string> clientIds, CLientMessage<T> message) => Send(_context.Clients.GroupExcept(_groupName, clientIds), message);

        public Task SendToClient<T>(string clientId, CLientMessage<T> message) => Send(_context.Clients.Client(clientId), message);

        public Task RegisterClient(string clientId) => _context.Groups.AddToGroupAsync(_groupName, clientId);
        public Task DeRegisterClient(string clientId) => _context.Groups.RemoveFromGroupAsync(_groupName, clientId);
    }
}
