using Microsoft.AspNetCore.ResponseCompression;
using WarOfTheWordle.Hubs;
using WarOfTheWordle.Services;
using WarOfTheWordle.Services.Game;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();

builder.Services.AddResponseCompression(opts =>
{
    opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
        new[] { "application/octet-stream" });
});


#region ioc

//builder.Services.AddSingleton<IWordService, TestWordService>();
builder.Services.AddSingleton<IWordService, FileBackedWordService>();

builder.Services.AddSingleton<IGameService, SingleGameService>();

//builder.Services.AddSingleton<IClientDispatcher>(services => new SignalRGroupesClientDispatcher("test", services.GetRequiredService<Microsoft.AspNetCore.SignalR.IHubContext<GameHub>>()));
builder.Services.AddSingleton<GameHub>();
builder.Services.AddSingleton<IClientDispatcher, SignalClientDispatcher>();


#endregion
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
}


app.UseStaticFiles();

app.UseRouting();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");

app.MapHub<GameHub>(WarOfTheWordle.Constants.SignalRConstants.HubAddress);
app.MapHub<ChatHub>("/chathub");

app.Run();
